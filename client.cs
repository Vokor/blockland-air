//pref's
$blAir::root = "Add-Ons/System_blAir";

//if this doesn't work, is there some way we can get the git version?
//if(isFile("Add-Ons/System_blAir/.git/refs/heads/master"))
//{

	//%file = new fileObject();
	//%filename = "Add-Ons/System_blAir/.git/refs/heads/master";
	//%file.openForRead(%filename);
	//%line = %file.readLine();
	//$blAir::version = "git-" @ getSubStr(%line,0,7);
//}
//else
//{

	$blAir::version = "Beta";

//}

//copying fonts over
function blAir_getFonts(%this)
{
	for(%i = findFirstFile("Add-Ons/System_blAir/fonts/*.gft"); %i !$= ""; %i = findNextFile("Add-Ons/System_blAir/fonts/*.gft"))
	{
		%fileName = filename(%i);
		if(!isFile("base/client/ui/cache/" @ %fileName))
		{
			fileCopy(%i,"base/client/ui/cache/" @ %fileName);
		}
	}
}
blAir_getfonts();

// Load Modules
exec($blAir::root @ "/modules/profiles.cs");

// Replace Main Menu
exec($blAir::root @ "/gui/blAir_mm.gui");
exec($blAir::root @ "/script/blAir.mm.cs");

// Load topbar
exec($blAir::root @ "/gui/blAir_bar.gui");
exec($blAir::root @ "/script/blAir.bar.cs");

// Loading Gui
exec($blAir::root @ "/gui/blAir_load.gui");
exec($blAir::root @ "/script/blAir.load.cs");

// In-Game things
//exec($blAir::root @ "/script/blAir.ingame.cs");

function blAir_init()
{
	mainMenuGui.setName("oldMainMenu");
	blAir_mm.setName("mainMenuGui");
	MainMenuButtonsGui.delete();
	//mm_fade.setName("oldmm_fade");
	//blAir_BG.setName("mm_fade");
	canvas.pushDialog("mainMenuGui");
	canvas.pushDialog("blAir_mm_buttons");
	blAir_mm_buttons.add(blAir_bar);

	//adding authBar and versions
	oldMainMenu.remove(MM_AuthBar);
	blAir_mm_buttons.add(MM_AuthBar);
	MM_AuthText.setProfile(blAir_authTextProfile);
	MM_AuthKeyButton.setProfile(blAir_buttonProfile);
	MM_AuthKeyButton.setbitMap("base/client/ui/button");
	MM_AuthNameButton.setProfile(blAir_buttonProfile);
	MM_AuthNameButton.setbitMap("base/client/ui/button");
	MM_AuthRetryButton.setProfile(blAir_buttonProfile);
	MM_AuthRetryButton.setbitMap("base/client/ui/button");
	MM_AuthBar.setBitmap($blAir::root @ "/images/authbar.png");

	//delete rtb version if no rtb
	if(!$RTB::Version)
	{
		blAir_mm_buttons_rtbV.delete();
	}

}

schedule(2000, 0, blAir_init);
