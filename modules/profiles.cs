new GuiControlProfile(blAir_AuthTextProfile : GuiMLTextProfile) 
{
   fontType = "Open Sans Bold";
   fontSize = "16";
   fontColors[0] = "255 255 255 200";
   fontColors[1] = "255 255 255 200";
   fontColors[2] = "255 255 255 200";
   fontColors[3] = "255 255 255 200";
   fontColors[4] = "255 255 255 200";
   fontColors[5] = "255 255 255 200";
   fontColors[6] = "255 255 255 200";
   fontColors[7] = "255 255 255 200";
   fontColors[8] = "255 255 255 200";
   fontColors[9] = "255 255 255 200";
   fontColor = "255 255 255 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "255 255 255 255";
   fontColorSEL = "255 255 255 200";
   fontColorLink = "255 255 255 200";
   fontColorLinkHL = "255 255 255 200";
   doFontOutline = "0";
   fontOutlineColor = "70 70 70 255";
   justify = "center";
   textOffset = "3 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
};

new GuiControlProfile(blAir_VersionTextProfile_BL : GuiMLTextProfile) 
{
   fontType = "Open Sans Bold";
   fontSize = "16";
   fontColors[0] = "255 255 255 200";
   fontColors[1] = "255 255 255 200";
   fontColors[2] = "255 255 255 200";
   fontColors[3] = "255 255 255 200";
   fontColors[4] = "255 255 255 200";
   fontColors[5] = "255 255 255 200";
   fontColors[6] = "255 255 255 200";
   fontColors[7] = "255 255 255 200";
   fontColors[8] = "255 255 255 200";
   fontColors[9] = "255 255 255 200";
   fontColor = "255 255 255 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "255 255 255 255";
   fontColorSEL = "255 255 255 200";
   fontColorLink = "255 255 255 200";
   fontColorLinkHL = "255 255 255 200";
   doFontOutline = "1";
   fontOutlineColor = "0 0 255 255";
   justify = "right";
   textOffset = "3 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   bitmap = "base/client/ui/BlockWindow";
};

new GuiControlProfile(blAir_VersionTextProfile_RTB : GuiMLTextProfile) 
{
   fontType = "Open Sans Bold";
   fontSize = "16";
   fontColors[0] = "255 255 255 200";
   fontColors[1] = "255 255 255 200";
   fontColors[2] = "255 255 255 200";
   fontColors[3] = "255 255 255 200";
   fontColors[4] = "255 255 255 200";
   fontColors[5] = "255 255 255 200";
   fontColors[6] = "255 255 255 200";
   fontColors[7] = "255 255 255 200";
   fontColors[8] = "255 255 255 200";
   fontColors[9] = "255 255 255 200";
   fontColor = "255 255 255 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "255 255 255 255";
   fontColorSEL = "255 255 255 200";
   fontColorLink = "255 255 255 200";
   fontColorLinkHL = "255 255 255 200";
   doFontOutline = "1";
   fontOutlineColor = "255 0 0 255";
   justify = "right";
   textOffset = "3 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   bitmap = "base/client/ui/BlockWindow";
};

new GuiControlProfile(blAir_VersionTextProfile_blAir : GuiMLTextProfile) 
{
   fontType = "Open Sans Bold";
   fontSize = "16";
   fontColors[0] = "255 255 255 200";
   fontColors[1] = "255 255 255 200";
   fontColors[2] = "255 255 255 200";
   fontColors[3] = "255 255 255 200";
   fontColors[4] = "255 255 255 200";
   fontColors[5] = "255 255 255 200";
   fontColors[6] = "255 255 255 200";
   fontColors[7] = "255 255 255 200";
   fontColors[8] = "255 255 255 200";
   fontColors[9] = "255 255 255 200";
   fontColor = "255 255 255 255";
   fontColorHL = "255 255 255 255";
   fontColorNA = "255 255 255 255";
   fontColorSEL = "255 255 255 200";
   fontColorLink = "255 255 255 200";
   fontColorLinkHL = "255 255 255 200";
   doFontOutline = "1";
   fontOutlineColor = "25 170 25 255";
   justify = "right";
   textOffset = "3 0";
   autoSizeWidth = "0";
   autoSizeHeight = "0";
   returnTab = "0";
   numbersOnly = "0";
   cursorColor = "0 0 0 255";
   bitmap = "base/client/ui/BlockWindow";
};

new GuiControlProfile(blAirGlassWindowProfile)
{
   opaque = true;
   border = 0;
   justify = "center";
   fillColor = "255 255 255 12";
   text = "GuiWindowCtrl test";
   fontColor = "255 255 255 255";
   fontSize = 12;
   fontType = "Verdana";
   bitmap = $blAir::root @ "/images/misc/glassWindowArray.png";
   hasBitmapArray = true;
};

new GuiControlProfile(blAir_ButtonProfile : blockButtonProfile)
{
   fontColor = "255 255 255 200";
   fontType = "Open Sans Bold";
   fontSize = "16";
   justify = "Center";
   fontColors[1] = "255 255 255 200";
   fontColors[2] = "255 255 255 200";  
   fontColors[3] = "255 255 255 200"; 
   fontColors[4] = "255 255 255 200"; 
   fontColorLink = "255 255 255 200";
   fontColorLinkHL = "255 255 255 200";
   bitmap = "base/client/ui/button";
};
