
//Fetching Preview
//thanks to Plornt for helping me do this :)
function NLGUI_PreviewTCP::online(%this,%line)
{
	if(%this.getnext)
	{
		%this.setBinary(true);
		return;
	}

	if(%line $= "")
	{
		%this.getnext = true;
	}
}

function NLGUI_PreviewTCP::onBinChunk(%this,%bin)
{
	cancel(%this.timeout);
	%this.timeout = %this.schedule(500,savePreview);
}

function NLGUI_PreviewTCP::savePreview(%this)
{
	%this.savedPreview = true;
	%this.saveBufferToFile("config/client/blAir/NLGUI/blAir_ServerP_" @ %this.port @ ".jpg");

	blAir_load_top_previewImage.bitmap = "config/client/blAir/NLGUI/blAir_serverP_" @ %this.port @ ".jpg";
	blAir_load_top.remove(blAir_load_top_previewImage);
	blAir_load_top.add(blAir_load_top_previewImage);
}

function NLGUI_PreviewTCP::onConnected(%this)
{
	%this.getPreview(%this.ip,%this.port);
}

function NLGUI_PreviewTCP::getPreview(%this,%ip,%port,%blid)
{
	%uri = strreplace(%ip,".","-") @ "_" @ %port;
	%data = "q=" @ %uri;
	%length = strLen(%data);
	%path = %this.path @ "?" @ %data @ " HTTP/1.1\r\n";
	%host = "Host: " @ %this.host @ "\r\n";
	%userAgent = "User-Agent: Torque/1.0\r\n";
	%type = "Content-Type: text/html\r\n";
	%length = "Content-Length:" @ %length @ "\r\n\r\n";
	%headers = %host @ %userAgent @ %type @ %length;
	%this.send("GET" SPC %path @ %headers @ %data);
	%this.binaryMode = 0;
	%this.ip = strreplace(%ip,".","-");
	%this.port = %port;
}

function NLGUI_PreviewTCP::onDisconnect(%this)
{
	%this.delete();
}


//init
function blAir_load_init()
{
	//Load_MapPicture.delete();
	canvas.pushDialog(blAir_load);
	canvas.pushDialog(blAir_load_top);
	loadingGui.remove(LoadingProgress);
	loadingGui.remove(LoadingSecondaryProgress);
	blAir_load_top.add(blAir_bar);
	blAir_load_top.add(LoadingProgress);
	blAir_load_top.add(LoadingSecondaryProgress);
	%extent1 = LoadingProgress.extent;
	LoadingProgress.resize(20,getWord(getRes(),1)-50,getWord(getRes(),0)-40,getWord(%extent1,1));
	LoadingSecondaryProgress.resize(20,getWord(getRes(),1)-50,getWord(getRes(),0)-40,getWord(%extent1,1));
	LoadingProgressTxt.setProfile(blAir_authTextProfile);

	%pre = new TCPObject(NLGUI_PreviewTCP);
	%pre.host = "image.blockland.us";
	%pre.path = "/detail.php";
	%pre.connect(%pre.host @ ":80");
	%pre.ip = serverConnection.getRawIP();
	%pre.port = mabs(getSubStr(serverConnection.getAddress(),strPos(serverConnection.getAddress(),":")+1,5));
	//%pre.port = 28000;

	loadingGui.add(newPlayerListGui);
	loadingGui.remove(newPlayerListGui);
	%title1 = NPL_Window.getValue();
	%title2 = getSubStr(%title1,strStr(%title1,"-")+2,strLen(%title1));
	blAir_load_top_serverName.setText("<shadow:5:5><shadowcolor:000000><font:Open Sans Bold:30><just:left><color:ffffff> " @ %title2);

	//doing the player list
	//%header = "<font:Open Sans Bold:16><color:ffffff>Name" TAB "Score" TAB "BL_ID" TAB "Trust" TAB "Admin";
	//blAir_load_top_text.setText("<tab:150,200,275,325>" @ %header);

	//schedule(1000, 0, blAir_addLoadLine);
}

function blAir_addLoadLine(%this)
{
	//player list
	%rows = NPL_List.rowCount();
	for(%i = 0; %i < %rows; %i++)
	{
		%line = NPL_List.getRowText(%i);
		%var4 = getField(%line, 3); 
		%var2 = getField(%line, 1);
		%var3 = getField(%line, 2);
		%var5 = getField(%line, 4);
		%var6 = getField(%line, 0);
		%color = "ffffff";

		if(%var6 $= "S")
		{
			%color = "FFFF00";
		}
		else if(%var6 $= "A")
		{
			%color = "ADD8E6";
		}

		if(%var5 $= "You")
		{
			%color = "FFA500";
		}

		%line = "<color:" @ %color @ ">" @ %var2 @ "" TAB %var3 TAB %var4 TAB %var5 TAB %var6;//<font:Open Sans:16>

		//schedule(1000, 0, blAir_addLoadLine, %line);
		blAir_load_top_text.setText(blAir_load_top_text.getText() NL %line);
	}
}


function blAir_load_BG::onDone(%this)
{
	%this.pic++;
	%a = %this.pic;
	if(%a > 7)
	{
		%a = 1;
		%this.pic = 1;
	}
	%this.setBitmap($blAir::root @ "/images/bg/bg" @ %a @ ".jpg");
	blAir_load.remove(blAir_load_BG);
	blAir_load.add(blAir_load_BG);
}




//package
package blAir_load_package
{
	function LoadingGui::onWake(%this,%obj)
	{
		blAir_load_init();
		parent::onWake(%this,%obj);
	}

	function secureClientCmd_clientJoin(%this, %a, %b, %c, %d)
	{
		parent::secureClientCmd_clientJoin(%this, %a, %b, %c, %d);

		loadingGui.add(newPlayerListGui);
		loadingGui.remove(newPlayerListGui);
		%title1 = NPL_Window.getValue();
		%title2 = getSubStr(%title1,strStr(%title1,"-")+2,strLen(%title1));
		blAir_load_top_serverName.setText("<shadow:5:5><shadowcolor:000000><font:Open Sans Bold:30><just:left><color:ffffff> " @ %title2);

		//doing the player list
		%header = "<font:Open Sans Bold:16><color:ffffff>Name" TAB "Score" TAB "BL_ID" TAB "Trust" TAB "Admin";
		blAir_load_top_text.setText("<tab:150,200,275,325>" @ %header);

		schedule(1000, 0, blAir_addLoadLine);
	}
};
activatePackage(blAir_load_package);