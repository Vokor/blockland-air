function blAir_timeLoop()
{
	cancel($blAir::TimeLoop);
	
	%time = getSubStr(getDateTime(),9,5);
	%len = strLen(%time);

		%m = " AM";
		%firstColon = strPos(%time,":");
		%hours = getSubStr(%time,0,%firstcolon);
		if(%hours > 12)
		{
			%hours = %hours - 12;
			%m = " PM";
			%time = %hours @ getSubStr(%time,%firstColon,%len);
		}
		
	blAir_bar_centerText.setText("<shadow:2:2><shadowcolor:000000><font:Open Sans Bold:16><just:center><color:ffffff>" @ %time @ %m @ "");
	//blAir_AuthText.setText(MM_AuthText.getValue());

	if(isObject(serverConnection))
	{
		%ping = serverConnection.getPing();
	}
	
	if(%ping !$= "")
	{
		%color = "FFFFFF";
		if(%ping < 70)
		{
			%color = "00ff00";
		}
		else if(%ping > 70 && %ping < 120)
		{
			%color = "FFFF00";
		}
		else if(%ping > 120)
		{
			%color = "FF0000";
		}
		blAir_bar_rightText.settext("<shadow:2:2><shadowcolor:000000><font:Open Sans Bold:16><just:right><color:ffffff>Ping:<color:" @ %color @ "> " @ %ping @ "<color:ffffff> ms   ");
	}
	else
	{
		blAir_bar_rightText.setText("");
	}

	$blAir::TimeLoop = schedule(1000, 0, blAir_timeLoop);
}

blAir_timeLoop();


package blAir_bar_package
{
	function LoadingGui::onWake(%this,%obj)
	{
		if(!blAir_bar.isAwake())
		{
			LoadingGui.add(blAir_bar);
		}

		parent::onWake(%this,%obj);

		loadingGui.add(newPlayerListGui);
		loadingGui.remove(newPlayerListGui);
		%title1 = NPL_Window.getValue();
		%title2 = getSubStr(%title1,strStr(%title1,"-")+2,strLen(%title1));
		blAir_bar_leftText.setText("<shadow:2:2><shadowcolor:000000><font:Open Sans Bold:16><just:left><color:ffffff> " @ %title2);
	}

	function PlayGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);


		if(!blAir_bar.isAwake())
		{
			PlayGui.add(blAir_bar);
		}

		PlayGui.add(newPlayerListGui);
		PlayGui.remove(newPlayerListGui);
		%title1 = NPL_Window.getValue();
		%title2 = getSubStr(%title1,strStr(%title1,"-")+2,strLen(%title1));
		blAir_bar_leftText.setText("<shadow:2:2><shadowcolor:000000><font:Open Sans Bold:16><just:left><color:ffffff> " @ %title2);
	}

	function mainMenuGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);

		blAir_mm_buttons.add(blAir_bar);

		//blAir_bar_leftText.setText("<shadow:2:2><shadowcolor:000000><font:Open Sans Bold:16><just:left><color:ffffff> Main Menu");
	}

	function newChatText::onWake(%this,%obj)
	{
		if(blAir_bar.isAwake())
		{
			%extent = %this.extent;
			%this.resize(2,40,getWord(%extent,0),getWord(%extent,1));
		}
		parent::onWake(%this,%obj);
	}

	function chatWhosTalkingText::onWake(%this,%obj)
	{
		if(blAir_bar.isAwake())
		{
			%extent = %this.extent;
			%this.resize(0,25,getWord(%extent,0),getWord(%extent,1));
		}
		parent::chatWhosTalkingText(%this,%obj);
	}

	function playGUI::hideToolBox(%q, %obj, %b, %a)
	{
		parent::hideToolBox(%q, %obj, %b, %a);

		if(blAir_bar.isAwake())
		{
			%this = Hud_ToolBox;
			%extent = %this.extent;
			%position = %this.position;

			if(getWord(%position, 1) $= "0")
			{
				%this.resize(getWord(%position, 0),getWord(%position, 1)+23,getWord(%extent,0),getWord(%extent,1));
			}

			%this = Hud_ToolNameBG;
			%extent = %this.extent;

			if(getWord(%positon, 1) $= "0" || getWord(%position, 1) $= "320")
			{
				%this.resize(getWord(%position, 0),getWord(%position, 1)+23,getWord(%extent,0),getWord(%extent,1));
			}

			if(!blAir_bar.add)
			{
				PlayGui.remove(blAir_bar);
				playGUI.add(blAir_bar);
				blAir_bar.add = 1;
			}
		}
	}
};
activatePackage(blAir_bar_package);