package blAir_SprayCan
{

	//tried to remove the sliding effect
	function useSprayCan(%a)
	{

		parent::useSprayCan(%a);
		HUD_PaintBox.setVisible(1);

	}

	//goes with the above
	//doing a trace(1); when pulling out the spraycan and bringing the tools up shows what commands are being used.
	function PlayGui::hidePaintBox(%a,%b,%c,%d)
	{

		parent::hidePaintBox(%a,%b,%c,%d);
		HUD_PaintBox.setVisible(0);

	}

	
	//this isn't working right? but i know the positions work and everything.
	//supposed to be dynamic based on the colorset size.
	function HUD_PaintBox::onWake(%this,%obj)
	{

		parent::onWake(%this,%obj);
		for(%i=0;%i<HUD_PaintBox.getCount();%i++)
		{
			%ob = HUD_PaintBox.getObject(%i);

			//removes most of the images... that aren't named.
			if(%ob.profile $= "HUDBitmapProfile")
			{
				%ob.position = "-1000 0";

			}

			//screw badspot for not naming GUIs
			//this is the semi-transparent black background behind the colorset.
			if(%i == 4)
			{

				%paintbox_width = getWord(%ob.extent,0);

			}

		}
		Tooltip_Paint.position = "-1000 0";
		HUD_PaintNameBG.position = "-1000 0";
		//86 154 = extent (math weeeeee)
		//800 600 = screen
		//714 6 = position
		HUD_PaintBox.position = getWord(getRes(),0)-%paintbox_width SPC "6";

	}

};
activatePackage(blAir_SprayCan);