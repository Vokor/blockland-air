package blAir_Override
{
	function GameModeGui::clickBack(%this)
	{
		canvas.PopDialog(%this.getName());
	}
};
activatePackage(blAir_Override);


function blAir_BG::onDone(%this)
{
	%this.pic++;
	%a = %this.pic;
	if(%a > 7)
	{
		%a = 1;
		%this.pic = 1;
	}
	%this.setBitmap($blAir::root @ "/images/bg/bg" @ %a @ ".jpg");
	mainMenuGui.remove(blAir_BG);
	mainMenuGui.add(blAir_BG);
}

function blAir_mm_buttons::showButtons(%this)
{
	canvas.pushDialog(blAir_mm_buttons);
	blAir_mm_buttons.add(blAir_bar);
	blAir_mm_buttons.add(MM_AuthBar);
	blAir_bar_leftText.setText("<shadow:2:2><shadowcolor:00000066><font:Open Sans:16><just:left><color:ffffff> Main Menu");
}

package blAir_mm_package
{
	function LoadingGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);
	}

	function PlayGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);
	}

	function mainMenuGui::onWake(%this,%obj)
	{
		parent::onWake(%this,%obj);

		blAir_mm_buttons.showbuttons();
	}

	function blAir_BG1::onWake(%this)
	{
		//parent::onWake(%this);
		blAir_mm_buttons.showbuttons();
	}

	function MM_AuthBar::blinkSuccess(%this)
	{
		blAir_mm_buttons.add(%this);
		%this.setBitmap($blAir::root @ "/images/authbar.png");
		parent::blinkSuccess(%this);
	}
};
activatePackage(blAir_mm_package);